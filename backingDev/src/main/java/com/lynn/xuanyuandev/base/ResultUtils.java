package com.lynn.xuanyuandev.base;

import java.io.Serializable;

/**
 * 响应工具类
 * @Author Lynn
 * @Date 2021/10/9
 **/
public class ResultUtils<I extends Serializable> {
    
    /**
     * TODO 返回成功
     * @author Lynn
     * @date 2021/10/9 1:48 
     * @return com.lynn.xuanyuandev.base.BaseResponse<T>
     */
    public  static <T> BaseResponse<T> fine(T data)
    {
        return new BaseResponse<>(0, data, "its fine");
    }

    /**
     * TODO 错误
     * @author Lynn
     * @date 2021/10/9 1:51
     * @return com.lynn.xuanyuandev.base.BaseResponse<T>
     */
    public static <T> BaseResponse<T> error(int code, String errorMessage)
    {
        return new BaseResponse<>(code, null, errorMessage);
    }

}
