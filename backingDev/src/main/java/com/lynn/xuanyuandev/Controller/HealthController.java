package com.lynn.xuanyuandev.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 查看网页是否成功运行
 * @Author Lynn
 * @Date 2021/10/7
 **/
@RestController
@RequestMapping("/health")
public class HealthController
{

    @GetMapping
    public String checkHealth()
    {
        return "Success";
    }
}
