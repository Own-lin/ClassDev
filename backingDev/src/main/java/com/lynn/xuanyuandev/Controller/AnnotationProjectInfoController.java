package com.lynn.xuanyuandev.Controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lynn.xuanyuandev.base.BaseResponse;
import com.lynn.xuanyuandev.base.ResultUtils;
import com.lynn.xuanyuandev.model.entity.AnnotationProjectInfo;
import com.lynn.xuanyuandev.service.AnnotationProjectInfoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Author Lynn
 * @Date 2021/10/8
 **/
@RestController
@RequestMapping(value = "/annoInfo")
public class AnnotationProjectInfoController
{
    @Resource
    private AnnotationProjectInfoService annoInfoService;

    /**
     * TODO 保存标出文件的
     * @author Lynn
     * @date 2021/10/7 23:27
     * @return int 返回输入的id
     */
    @PostMapping(value = "add")
    public BaseResponse<String> saveAnnotationProjectInfo(AnnotationProjectInfo annoInfo)
    {
        if(annoInfo == null)
        {
            return ResultUtils.error(400, "参数为空");
        }
        annoInfoService.save(annoInfo);
        return ResultUtils.fine(annoInfo.getId());
    }

    /**
     * TODO 更新标注文件
     * @author Lynn
     * @date 2021/10/7 23:31
     * @return int
     */
    @PostMapping(value = "update")
    public BaseResponse<Boolean> updateAnnotationProjectInfo(AnnotationProjectInfo annoInfo)
    {
        if (annoInfo == null)
        {
            return ResultUtils.error(400, "参数为空");
        }
        return ResultUtils.fine(annoInfoService.updateById(annoInfo));
    }

    /**
     * TODO 搜索标注文件
     * @author Lynn
     * @date 2021/10/7 23:32
     * @return PageDTO<AnnotationProjectInfo>
     */
    @GetMapping(value = "search")
    public BaseResponse<IPage<AnnotationProjectInfo>> searchAnnotationProjectInfo(AnnotationProjectInfo annoInfo)
    {
        QueryWrapper<AnnotationProjectInfo> annoInfoQueryWrapper = new QueryWrapper<>(annoInfo);
        return ResultUtils.fine(annoInfoService.page(new Page<>(1, 10), annoInfoQueryWrapper  ));

    }


    /**
     * TODO 删除标注文件
     * @author Lynn
     * @date 2021/10/7 23:43
     * @return int
     */
    @PostMapping(value = "del")
    public BaseResponse<Boolean> deleteAnnotationProjectInfo(int id)
    {
        if (id < 1)
        {
            return ResultUtils.error(400, "参数为空");
        }

        return ResultUtils.fine(annoInfoService.removeById(id));
    }
}
