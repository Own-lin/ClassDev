package com.lynn.xuanyuandev.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 标注项目表
 * @author Lynn
 * @TableName annotation_project_info
 */
@TableName(value ="annotation_project_info")
@Data
@Getter
@Setter
public class AnnotationProjectInfo implements Serializable {
    /**
     * ID
     */
    @TableId
    private String id;

    /**
     * 创建人ID
     */
    @TableField(value = "create_by")
    private String createBy;

    /**
     * 创建人名称
     */
    @TableField(value = "create_name")
    private String createName;

    /**
     * 更新人ID
     */
    @TableField(value = "update_by")
    private String updateBy;

    /**
     * 更新人名称
     */
    @TableField(value = "update_name")
    private String updateName;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 删除标志
     */
    @TableLogic
    @TableField(value = "del_flag")
    private String del_flag;

    /**
     * 项目编号
     */
    @TableField(value = "project_no")
    private String projectNo;

    /**
     * 项目名称
     */
    @TableField(value = "project_name")
    private String projectName;

    /**
     * 描述
     */
    @TableField(value = "description")
    private String description;

    /**
     * 项目状态
     */
    @TableField(value = "project_status")
    private Integer projectStatus;

    /**
     * 标注类型
     */
    @TableField(value = "mark_type")
    private Integer markType;

    /**
     * 标签类型
     */
    @TableField(value = "label_type")
    private String labelType;

    /**
     * 是否团队项目
     */
    @TableField(value = "is_team_project")
    private Integer isTeamProject;

    /**
     * 项目管理员ID
     */
    @TableField(value = "project_manager_id")
    private String projectManagerId;

    /**
     * 项目管理员
     */
    @TableField(value = "project_manager")
    private String projectManager;

    /**
     * 标注指南ID
     */
    @TableField(value = "mark_guide_id")
    private String markGuideId;

    /**
     * 其他字段1
     */
    @TableField(value = "other_one")
    private String otherOne;

    /**
     * 其他字段2
     */
    @TableField(value = "other_two")
    private String otherTwo;

    /**
     * 其他字段3
     */
    @TableField(value = "other_three")
    private String otherThree;

    /**
     * 标注方案ID
     */
    @TableField(value = "mark_scheme_id")
    private String markSchemeId;

    /**
     * 租户ID
     */
    @TableField(value = "tenant_id")
    private String tenantId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        AnnotationProjectInfo other = (AnnotationProjectInfo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getCreateBy() == null ? other.getCreateBy() == null : this.getCreateBy().equals(other.getCreateBy()))
            && (this.getCreateName() == null ? other.getCreateName() == null : this.getCreateName().equals(other.getCreateName()))
            && (this.getUpdateBy() == null ? other.getUpdateBy() == null : this.getUpdateBy().equals(other.getUpdateBy()))
            && (this.getUpdateName() == null ? other.getUpdateName() == null : this.getUpdateName().equals(other.getUpdateName()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDel_flag() == null ? other.getDel_flag() == null : this.getDel_flag().equals(other.getDel_flag()))
            && (this.getProjectNo() == null ? other.getProjectNo() == null : this.getProjectNo().equals(other.getProjectNo()))
            && (this.getProjectName() == null ? other.getProjectName() == null : this.getProjectName().equals(other.getProjectName()))
            && (this.getDescription() == null ? other.getDescription() == null : this.getDescription().equals(other.getDescription()))
            && (this.getProjectStatus() == null ? other.getProjectStatus() == null : this.getProjectStatus().equals(other.getProjectStatus()))
            && (this.getMarkType() == null ? other.getMarkType() == null : this.getMarkType().equals(other.getMarkType()))
            && (this.getLabelType() == null ? other.getLabelType() == null : this.getLabelType().equals(other.getLabelType()))
            && (this.getIsTeamProject() == null ? other.getIsTeamProject() == null : this.getIsTeamProject().equals(other.getIsTeamProject()))
            && (this.getProjectManagerId() == null ? other.getProjectManagerId() == null : this.getProjectManagerId().equals(other.getProjectManagerId()))
            && (this.getProjectManager() == null ? other.getProjectManager() == null : this.getProjectManager().equals(other.getProjectManager()))
            && (this.getMarkGuideId() == null ? other.getMarkGuideId() == null : this.getMarkGuideId().equals(other.getMarkGuideId()))
            && (this.getOtherOne() == null ? other.getOtherOne() == null : this.getOtherOne().equals(other.getOtherOne()))
            && (this.getOtherTwo() == null ? other.getOtherTwo() == null : this.getOtherTwo().equals(other.getOtherTwo()))
            && (this.getOtherThree() == null ? other.getOtherThree() == null : this.getOtherThree().equals(other.getOtherThree()))
            && (this.getMarkSchemeId() == null ? other.getMarkSchemeId() == null : this.getMarkSchemeId().equals(other.getMarkSchemeId()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getCreateBy() == null) ? 0 : getCreateBy().hashCode());
        result = prime * result + ((getCreateName() == null) ? 0 : getCreateName().hashCode());
        result = prime * result + ((getUpdateBy() == null) ? 0 : getUpdateBy().hashCode());
        result = prime * result + ((getUpdateName() == null) ? 0 : getUpdateName().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDel_flag() == null) ? 0 : getDel_flag().hashCode());
        result = prime * result + ((getProjectNo() == null) ? 0 : getProjectNo().hashCode());
        result = prime * result + ((getProjectName() == null) ? 0 : getProjectName().hashCode());
        result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
        result = prime * result + ((getProjectStatus() == null) ? 0 : getProjectStatus().hashCode());
        result = prime * result + ((getMarkType() == null) ? 0 : getMarkType().hashCode());
        result = prime * result + ((getLabelType() == null) ? 0 : getLabelType().hashCode());
        result = prime * result + ((getIsTeamProject() == null) ? 0 : getIsTeamProject().hashCode());
        result = prime * result + ((getProjectManagerId() == null) ? 0 : getProjectManagerId().hashCode());
        result = prime * result + ((getProjectManager() == null) ? 0 : getProjectManager().hashCode());
        result = prime * result + ((getMarkGuideId() == null) ? 0 : getMarkGuideId().hashCode());
        result = prime * result + ((getOtherOne() == null) ? 0 : getOtherOne().hashCode());
        result = prime * result + ((getOtherTwo() == null) ? 0 : getOtherTwo().hashCode());
        result = prime * result + ((getOtherThree() == null) ? 0 : getOtherThree().hashCode());
        result = prime * result + ((getMarkSchemeId() == null) ? 0 : getMarkSchemeId().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", createBy=").append(createBy);
        sb.append(", createName=").append(createName);
        sb.append(", updateBy=").append(updateBy);
        sb.append(", updateName=").append(updateName);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", delFlag=").append(del_flag);
        sb.append(", projectNo=").append(projectNo);
        sb.append(", projectName=").append(projectName);
        sb.append(", description=").append(description);
        sb.append(", projectStatus=").append(projectStatus);
        sb.append(", markType=").append(markType);
        sb.append(", labelType=").append(labelType);
        sb.append(", isTeamProject=").append(isTeamProject);
        sb.append(", projectManagerId=").append(projectManagerId);
        sb.append(", projectManager=").append(projectManager);
        sb.append(", markGuideId=").append(markGuideId);
        sb.append(", otherOne=").append(otherOne);
        sb.append(", otherTwo=").append(otherTwo);
        sb.append(", otherThree=").append(otherThree);
        sb.append(", markSchemeId=").append(markSchemeId);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}