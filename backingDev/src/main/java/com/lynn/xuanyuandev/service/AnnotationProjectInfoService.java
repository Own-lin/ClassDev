package com.lynn.xuanyuandev.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lynn.xuanyuandev.model.entity.AnnotationProjectInfo;

/**
*
 * @author Lynn
 */
public interface AnnotationProjectInfoService extends IService<AnnotationProjectInfo> {

}
