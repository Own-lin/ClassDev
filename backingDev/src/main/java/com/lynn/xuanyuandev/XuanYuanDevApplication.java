package com.lynn.xuanyuandev;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author Lynn
 */
@MapperScan("com.lynn.xuanyuandev.mapper")
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class XuanYuanDevApplication {

    public static void main(String[] args) {
        SpringApplication.run(XuanYuanDevApplication.class, args);
    }

}
