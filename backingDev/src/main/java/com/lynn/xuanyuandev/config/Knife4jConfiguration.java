package com.lynn.xuanyuandev.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * @author Lynn
 */
@Configuration
@EnableSwagger2WebMvc
public class Knife4jConfiguration {

    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("backing 接口文档")
                        .description("backingDev  文档")
                        .termsOfServiceUrl("https://github.com/Own-lin/ClassDev")
                        .version("1.0")
                        .build())
                //分组名称
                .groupName("2.9版本")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.lynn.xuanyuandev.Controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }
}