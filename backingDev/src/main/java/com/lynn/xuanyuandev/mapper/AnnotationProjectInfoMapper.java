package com.lynn.xuanyuandev.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lynn.xuanyuandev.model.entity.AnnotationProjectInfo;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Lynn
 * @Entity com.lynn.xuanyuandev.model.entity.AnnotationProjectInfo
*/
@Mapper
public interface AnnotationProjectInfoMapper extends BaseMapper<AnnotationProjectInfo> {


}
